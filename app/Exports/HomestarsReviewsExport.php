<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HomestarsReviewsExport implements FromCollection, WithHeadings
{

    public function __construct(Array $reviews){
      $this->reviews = $reviews;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->reviews);
    }

    public function headings(): array{
      return [
        'Author', 'Rating', 'Rating out of', 'Location', 'Link', 'Date'
      ];
    }
}
