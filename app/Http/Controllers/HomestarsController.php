<?php

namespace App\Http\Controllers;

use App\Exports\HomestarsReviewsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

use Goutte;
use Log;

class HomestarsController extends Controller
{

  public function __construct(Excel $excel){
    $this->excel = $excel;
    $this->reviews = [];
  }

  public function index(){
    return view('homestars');
  }

  public function fetchReviews(Request $request){

    // Set default values
    $startPageNumber = 1;
    $endPageNumber = 1;
    $url = 'https://homestars.com/companies/2593758-verdun-doors-windows?reviews_page=';
    $filename = 'homestars';

    // Set All Parameters if they are entered
    if(!is_null($request->input('start-page'))){
      $startPageNumber = intval($request->input('start-page'));
    }

    if(!is_null($request->input('end-page'))){
      $endPageNumber = intval($request->input('end-page'));
    }

    if(!is_null($request->input('filename'))){
      $filename = $request->input('filename');
    }

    if(!is_null($request->input('brand'))){

      switch ($request->input('brand')){
        case 'verdun' :
          break;

        case 'cc' :
          $url = 'https://homestars.com/companies/218473-consumers-choice-home-improvements?reviews_page=';
          break;

        case 'bh' :
          $url = 'https://homestars.com/companies/197665-beverley-hills-home-improvements?reviews_page=';
          break;

        case 'nc' :
          $url = 'https://homestars.com/companies/2783062-northern-comfort-windows-doors-ltd?reviews_page=';
          break;

        default : break;
      }

    }

    // Initiate an empty Reviews array
    $reviews = [];

    for ($i = $startPageNumber; $i < $endPageNumber + 1 ; $i++) {
      $this->crawl($url . $i . '&');
    }

    return $this->excel->download(new HomestarsReviewsExport($this->reviews), $filename . '.xlsx');

  } // END OF fetchReviews()

  private function crawl($url){

    $reviewsArray = [];
    $crawler = Goutte::request('GET', $url);

    $authors = $crawler->filter('.review .review-author__name  > a')->extract('_text');
    $ratings = $crawler->filter('.review .review-rating__rating')->extract('_text');
    $locations = $crawler->filter('.review .review-author__location')->extract('_text');
    $links = $crawler->filter('.review .review-content__title a')->extract('href');
    $dates = $crawler->filter('.review .review-content__date')->extract('_text');

    for ($i=0; $i < sizeof($authors); $i++) {

      $explodedRatings = explode('/', $ratings[$i]);

      $review = [
        'author' => $authors[$i],
        'rating' => preg_replace('/[^0-9]/', '', $explodedRatings[0]),
        'rating_out_of' => preg_replace('/[^0-9]/', '', $explodedRatings[1]),
        'location' => $locations[$i],
        'link' => 'https://homestars.com' . $links[$i],
        'date' => str_replace('/n', '', $dates[$i]),
      ];

      $this->reviews[] = $review;

    }

  } // END OF crawl()


}
