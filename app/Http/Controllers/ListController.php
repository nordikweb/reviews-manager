<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\HomestarsReviewsExport;
use Goutte;
use Log;

class ListController extends Controller
{

    public function __construct(Excel $excel){
      $this->excel = $excel;
      $this->reviews = [];
    }

    public function fetchReviews(Request $request){

      

      $startPageNumber = 1;
      $endPageNumber = 92;

      // Initiate an empty Reviews array
      $reviews = [];

      for ($i = $startPageNumber; $i < $endPageNumber + 1 ; $i++) {
        // Log::info($i . "------------------------------------------------------------");

        // Verdun
        // $newReviews = $this->crawl('https://homestars.com/companies/2593758-verdun-doors-windows?reviews_page=' . $i . '&service_area=1606962');

        // Consumer's Choice
        // $newReviews = $this->crawl('https://homestars.com/companies/218473-consumers-choice-home-improvements?reviews_page=' . $i . '&');

        // Consumer's Choice
        $newReviews = $this->crawl('https://homestars.com/companies/2783062-northern-comfort-windows-doors-ltd?reviews_page=' . $i . '&');


      }

      return $this->excel->download(new HomestarsReviewsExport($this->reviews), 'homestars.xlsx');

    } // END OF fetchReviews()

    private function crawl($url){

      $reviewsArray = [];
      $crawler = Goutte::request('GET', $url);

      $authors = $crawler->filter('.review .review-author__name  > a')->extract('_text');
      $ratings = $crawler->filter('.review .review-rating__rating')->extract('_text');
      $locations = $crawler->filter('.review .review-author__location')->extract('_text');
      $links = $crawler->filter('.review .review-content__title a')->extract('href');
      $dates = $crawler->filter('.review .review-content__date')->extract('_text');

      for ($i=0; $i < sizeof($authors); $i++) {

        $explodedRatings = explode('/', $ratings[$i]);

        $review = [
          'author' => $authors[$i],
          'rating' => preg_replace('/[^0-9]/', '', $explodedRatings[0]),
          'rating_out_of' => preg_replace('/[^0-9]/', '', $explodedRatings[1]),
          'location' => $locations[$i],
          'link' => 'https://homestars.com' . $links[$i],
          'date' => str_replace('/n', '', $dates[$i]),
        ];

        // Log::info($review);

        $this->reviews[] = $review;

      }

    } // END OF crawl()
}
