<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->date('review_date')->default(null)->nullable();
            $table->string('website')->default(null)->nullable();
            $table->integer('rating')->default(null)->nullable()->unsigned();
            $table->integer('rating_out_of')->default(null)->nullable()->unsigned();
            $table->string('location')->default(null)->nullable();
            $table->string('author')->default(null)->nullable();
            $table->string('url')->default(null)->nullable();
            $table->string('content')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
