<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Review Tools | Google </title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  </head>
  <body>
    <div class="flex-center-top position-ref full-height">

      <div class="content-list">
        <div class="title m-b-md">
          Google
        </div>

        <div class="links">
          <a href="{{ route('home') }}">Home</a>
          <a href="#">Google</a>
          <a href="#">Facebook</a>
        </div>

        <div class="export-tool">
          <div class="session">
            <div class="left">
              <?xml version="1.0" encoding="UTF-8"?>
              <svg enable-background="new 0 0 300 302.5" version="1.1" viewBox="0 0 300 302.5" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                <style type="text/css">
                .st01{fill:#fff;}
                </style>
                <path class="st01" d="m126 302.2c-2.3 0.7-5.7 0.2-7.7-1.2l-105-71.6c-2-1.3-3.7-4.4-3.9-6.7l-9.4-126.7c-0.2-2.4 1.1-5.6 2.8-7.2l93.2-86.4c1.7-1.6 5.1-2.6 7.4-2.3l125.6 18.9c2.3 0.4 5.2 2.3 6.4 4.4l63.5 110.1c1.2 2 1.4 5.5 0.6 7.7l-46.4 118.3c-0.9 2.2-3.4 4.6-5.7 5.3l-121.4 37.4zm63.4-102.7c2.3-0.7 4.8-3.1 5.7-5.3l19.9-50.8c0.9-2.2 0.6-5.7-0.6-7.7l-27.3-47.3c-1.2-2-4.1-4-6.4-4.4l-53.9-8c-2.3-0.4-5.7 0.7-7.4 2.3l-40 37.1c-1.7 1.6-3 4.9-2.8 7.2l4.1 54.4c0.2 2.4 1.9 5.4 3.9 6.7l45.1 30.8c2 1.3 5.4 1.9 7.7 1.2l52-16.2z"/>
              </svg>
            </div>
            <button id="authorize-button">Authorize</button>
            <button id="signout-button">Sign Out</button>
            <button id="accounts-button">Get Accounts</button>
            <button id="locations-button">Get Locations</button>
            <button id="reviews-button">Get Reviews</button>
            <form>
              @csrf


              <h4><span>Google</span> Review Scraper</h4>

              <div class="floating-label">
                <select name="brand" id="brand">
                  <option value="verdun">Verdun</option>
                  <option value="cc">Consumer's Choice</option>
                  <option value="nc">Northern Comfort</option>
                  <option value="bh">Beverley Hills</option>
                  <option value="nordik">Nordik</option>
                </select>
                <label for="brand">Brand</label>
              </div>

              <div class="floating-label">
                <input placeholder="Filename" type="text" name="filename" id="filename" autocomplete="off">
                <label for="filename">Filename:</label>
              </div>

              <div id="dynamic-content"> </div>

            <button id="submit" type="submit">Download XLSX</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnQa4VEgCgb7T3luo5nVvcXGXiUu8cA1A&libraries=places"></script>

  <script type="text/javascript">

      // Enter an API key from the Google API Console:
      //   https://console.developers.google.com/apis/credentials
      var apiKey = 'AIzaSyDEuVa_Uv3NI-BsLk1x3T9pYWrGzqCCu4s';

      // Enter a client ID for a web application from the Google API Console:
      //   https://console.developers.google.com/apis/credentials?project=_
      // In your API Console project, add a JavaScript origin that corresponds
      // to the domain where you will be running the script.
      var clientId = '1020200791007-asldr3l7fcfd3ulg271govn0kkmm24mv.apps.googleusercontent.com';

      // Use the latest Google My Business API version
      var gmb_api_version = 'https://mybusiness.googleapis.com/v4';

      // One or more authorization scopes. Additional scopes may be desired if
      // multiple APIs are used. Refer to the documentation for the API
      // or https://developers.google.com/people/v1/how-tos/authorizing
      // for details. At a minimum include the Google My Business authorization scope.
      var scopes = 'https://www.googleapis.com/auth/plus.business.manage';

      var authorizeButton = document.getElementById('authorize-button');
      var signoutButton = document.getElementById('signout-button');
      var accountsButton = document.getElementById('accounts-button');
      var locationsButton = document.getElementById('locations-button');
      var reviewsButton = document.getElementById('reviews-button');

      var accounts = [];
      var locations = [];

      function handleClientLoad() {
        // Load the API client and auth2 library
        gapi.load('client:auth2', initClient);
      }

      function initClient() {
        gapi.client.init({
            apiKey: apiKey,
            clientId: clientId,
            scope: scopes
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

          authorizeButton.onclick = handleAuthClick;
          signoutButton.onclick = handleSignoutClick;
          accountsButton.onclick = handleAccountsClick;
          locationsButton.onclick = handleLocationsClick;
          reviewsButton.onclick = handleReviewsClick;
        });
      }

      function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          authorizeButton.style.display = 'none';
          signoutButton.style.display = 'inline-block';
        } else {
          authorizeButton.style.display = 'inline-block';
          signoutButton.style.display = 'none';
        }
      }

      function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
      }

      function handleSignoutClick(event) {
        console.log('signout');
        gapi.auth2.getAuthInstance().signOut();
      }

      function handleAccountsClick(event) {
        console.log('accounts');
        let user = gapi.auth2.getAuthInstance().currentUser.get();
        let oauthToken = user.getAuthResponse().access_token;
        let req = gmb_api_version + '/accounts';
        let xhr = new XMLHttpRequest();
        let p = document.createElement('p');

        p.appendChild(document.createTextNode('Accounts'));

        console.log(req);

        xhr.responseType = 'json';
        xhr.open('GET', req);
        xhr.setRequestHeader('Authorization', 'Bearer ' + oauthToken);

        xhr.onload = function() {

          if (xhr.status != 200) {
            return;
          }

          console.log(xhr.response.accounts);
          for (let i = 0; i < xhr.response.accounts.length; i++) {

            let account = xhr.response.accounts[i].name;

            if (accounts.indexOf(account) === -1) {
              accounts.push(account);
            }

            p.appendChild(document.createElement('br'));
            p.appendChild(document.createTextNode(account));
            p.appendChild(document.createTextNode(' accountName: ' + xhr.response.accounts[i].accountName));
            p.appendChild(document.createTextNode(' type: ' + xhr.response.accounts[i].type));
            p.appendChild(document.createTextNode(' role: ' + xhr.response.accounts[i].role));
            p.appendChild(document.createTextNode(' state.status: ' + xhr.response.accounts[i].state.status));

            locationsButton.style.display = 'inline-block';
          }
        };
        xhr.send();
        document.getElementById('dynamic-content').appendChild(p);
      }

      function handleLocationsClick(event) {
        console.log('locations');
        let p = document.createElement('p');

        p.appendChild(document.createTextNode('Locations'));

        for (let i = 0; i < accounts.length; i++) {

          let user = gapi.auth2.getAuthInstance().currentUser.get();
          let oauthToken = user.getAuthResponse().access_token;
          let xhr = new XMLHttpRequest();
          let req = gmb_api_version + '/' + accounts[i] + '/locations';

          xhr.responseType = 'json';
          xhr.open('GET', req);
          xhr.setRequestHeader('Authorization', 'Bearer ' + oauthToken);

          xhr.onload = function() {

            if (xhr.status != 200 || xhr.response.locations == undefined) {
              return;
            }

            console.log(xhr.response.locations);

            for (let j = 0; j < xhr.response.locations.length; j++) {

              p.appendChild(document.createElement('br'));
              p.appendChild(document.createTextNode(xhr.response.locations[j].name));
              p.appendChild(document.createTextNode(' locationName: ' + xhr.response.locations[j].locationName));
              p.appendChild(document.createTextNode(' address.addressLines: ' + xhr.response.locations[j].address.addressLines));
              p.appendChild(document.createTextNode(' locationState isVerified: ' + xhr.response.locations[j].locationState.isVerified));
            }
          };
          xhr.send();
        }
        document.getElementById('dynamic-content').appendChild(p);
      }

      function handleReviewsClick(event){
        console.log('reviews');

        // Get the OAuth Token
        let user = gapi.auth2.getAuthInstance().currentUser.get();
        let oauthToken = user.getAuthResponse().access_token;

        let reviewData = {};

        // Initialize the Authorization header
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + oauthToken;


        axios.get(gmb_api_version + '/accounts').then(function(accountResponse){

          reviewData = accountResponse.data;

          let arrayOfAccountRequests = [];

          for (let account of reviewData.accounts) {
            arrayOfAccountRequests.push(axios.get(gmb_api_version + '/' + account.name + '/locations'));
          }

          return axios.all(arrayOfAccountRequests);

        }).then(function(locationResponse){

          for (var i = 0; i < reviewData.accounts.length; i++) {
            reviewData.accounts[i].locations = locationResponse[i].data.locations;
          }

          let arrayOfReviewRequests = [];

          for (let account of reviewData.accounts){

            for (let location of account.locations){
              arrayOfReviewRequests.push(axios.get(gmb_api_version + '/' + location.name + '/reviews'));
            }

          }

          return axios.all(arrayOfReviewRequests);

        }).then(function(reviewResponse){


        });



        // Get Accounts
        // axios.get(gmb_api_version + '/accounts').then(function(accResp){
        //
        //   reviewData = accResp.data;
        //
        //   for (let account of reviewData.accounts) {
        //
        //     // Get Locations
        //     axios.get(gmb_api_version + '/' + account.name + '/locations').then(function(locResp){
        //       account.locations = locResp.data.locations;
        //
        //       for (let location of account.locations){
        //
        //          // Get Reviews
        //         axios.get(gmb_api_version + '/' + location.name + '/reviews').then(function(revResp){
        //           location.reviews = revResp.data;
        //         });
        //
        //       }
        //
        //     });
        //
        //   }
        //
        //   axios.post('/fetch-google-reviews', reviewData).then(function(response){
        //     console.log(response);
        //   });
        //
        // });


      } // END OF handleReviewsClick()


    </script>

    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>

  </body>
</html>
