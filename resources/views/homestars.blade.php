<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>List of Reviews</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">

  </head>
  <body>
    <div class="flex-center-top position-ref full-height">

      <div class="content-list">
        <div class="title m-b-md">
          HomeStars
        </div>

        <div class="links">
          <a href="{{ route('home') }}">Home</a>
          <a href="{{ route('google') }}">Google</a>
          <a href="#">Facebook</a>
        </div>

        <div class="export-tool">
          <div class="session">
            <div class="left">
              <?xml version="1.0" encoding="UTF-8"?>
              <svg enable-background="new 0 0 300 302.5" version="1.1" viewBox="0 0 300 302.5" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                <style type="text/css">
                .st01{fill:#fff;}
                </style>
                <path class="st01" d="m126 302.2c-2.3 0.7-5.7 0.2-7.7-1.2l-105-71.6c-2-1.3-3.7-4.4-3.9-6.7l-9.4-126.7c-0.2-2.4 1.1-5.6 2.8-7.2l93.2-86.4c1.7-1.6 5.1-2.6 7.4-2.3l125.6 18.9c2.3 0.4 5.2 2.3 6.4 4.4l63.5 110.1c1.2 2 1.4 5.5 0.6 7.7l-46.4 118.3c-0.9 2.2-3.4 4.6-5.7 5.3l-121.4 37.4zm63.4-102.7c2.3-0.7 4.8-3.1 5.7-5.3l19.9-50.8c0.9-2.2 0.6-5.7-0.6-7.7l-27.3-47.3c-1.2-2-4.1-4-6.4-4.4l-53.9-8c-2.3-0.4-5.7 0.7-7.4 2.3l-40 37.1c-1.7 1.6-3 4.9-2.8 7.2l4.1 54.4c0.2 2.4 1.9 5.4 3.9 6.7l45.1 30.8c2 1.3 5.4 1.9 7.7 1.2l52-16.2z"/>
              </svg>
            </div>
            <form action="{{ route('fetchHomestarsReviews') }}" method="post">
              @csrf
              <h4><span>HomeStars</span> Review Scraper</h4>

              <div class="floating-label">
                <select name="brand" id="brand">
                  <option value="verdun">Verdun</option>
                  <option value="cc">Consumer's Choice</option>
                  <option value="nc">Northern Comfort</option>
                  <option value="bh">Beverley Hills</option>
                  <option value="nordik">Nordik</option>
                </select>
                <label for="brand">Brand</label>
              </div>

              <div class="floating-label">
                <input placeholder="Starting Page" type="number" name="start-page" id="start-page" autocomplete="off">
                <label for="start-page">Starting Page:</label>
              </div>

              <div class="floating-label">
                <input placeholder="Ending Page" type="number" name="end-page" id="end-page" autocomplete="off">
                <label for="end-page">Ending Page:</label>
              </div>

              <div class="floating-label">
                <input placeholder="Filename" type="text" name="filename" id="filename" autocomplete="off">
                <label for="filename">Filename:</label>
              </div>

            <button type="submit">Download XLSX</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>
