<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

// Homestars
Route::get('/homestars', 'HomestarsController@index')->name('homestars');
Route::post('/fetch-homestars-reviews', 'HomestarsController@fetchReviews')->name('fetchHomestarsReviews');

// Google
Route::get('/google', 'GoogleController@index')->name('google');
Route::post('/fetch-google-reviews', 'GoogleController@fetchReviews')->name('fetchGoogleReviews');

Route::get('/facebook', function () {
    return view('facebook');
});
